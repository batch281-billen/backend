// Function with parameters

function printName(name){
	console.log("My name is " + name);
};

printName("Juana");
printName("John");

let sampleVariable = "Bella";
printName(sampleVariable);

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
};

checkDivisibilityBy8(64);

// Functions as Arguments
function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed.");
};

function invokeFunction(argumentFunction){
	argumentFunction();
};

invokeFunction(argumentFunction);

// Using multiple parameters
function createFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
};

createFullName("Juan", "Perez", "Dela Cruz", "Hello");

// Using variables as arguments - yung firstName and the like is okay pa gamitin as variables (global) since sa parameters naman sya ginamit above. pero pag ginamit na as global variable, hindi na sya pwde ulitin if different approach.
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

// Return statement - allows us to output a value from a function to be passed to the line/block of code that invoked the function. Eto yung gumagawa ng structure para sa gusto makita na output pero hindi sya madidisplay or makikita. Then yung console.log na nasa baba ni return statement, hindi sya mababasa since lahat ng nasa baba ng return statement e madidisregard.

function returnFullName(firstName, middleName, lastName){
	return firstName + " " + middleName + " " + lastName;
	console.log("This message will not be printed.");
};

let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
console.log(completeName);
console.log(returnFullName(firstName, middleName, lastName));

function returnAddress(city, country){
	let fullAddress = city + ", " + country;
	return fullAddress;
};

let myAddress = returnAddress("Cebu city", "Philippines");
console.log(myAddress);