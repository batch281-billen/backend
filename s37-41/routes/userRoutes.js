const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for checking if the email already exists in the database.
// Invokes the checkEmailExists function from the controller file
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


// [My Solution] S38 Activity : Create a "/details" route that will accept the user's Id to retrieve the details of a user
/*
router.post("/details", (req, res) => {
	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController));
})
*/

// S38 ACTIVTY (Sir Tristan Solution)
// Route for retrieving user details
// revised post method to get method then input auth.verify
// The "auth.verify" acts as a middleware to ensure that the user is logged in before they can enroll to a course
router.get("/details", auth.verify, (req, res) => {

	// Uses decode method defined in the auth.js file to retrieve user information from the token passing the "token" from the request header
	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});


// Route to enroll user to a course [S41 Discussion]
/*
router.post("/enroll", (req, res) => {

	let data = {
		userId : req.body.userId,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))
});
*/


// [S41 ACTIVITY]

/*router.post("/enroll", auth.verify, (req, res) => {
	
	const data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		courseId : req.body.courseId
	}

	if(data.isAdmin == true) {
		userController.enroll(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
});*/


// SIR CHRIS RE-STRUCTURING OF CODE IN LINE WITH FULLSTACK NA
// Removed if else statement about isAdmin

router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		userData : auth.decode(req.headers.authorization),
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))
});


module.exports = router;