// We require express & mongoose to access express framework & mongoose library to MongoDB
const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const app = express(); //ini-istore sa variable na ito yung express application that will make backend creation easily


// Connect to our MongoDB
mongoose.connect("mongodb+srv://bvielbillen:FxbbfCP6kG4mI8ST@wdc028-course-booking.vrtxiuq.mongodb.net/s37-41API?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, "MongoDB connection Error."));
db.once('open', () => console.log('Now connected to MongoDB Atlas!'));

// Allows all resources to access our backend application
app.use(cors());

// This handles data coming from our client
app.use(express.json()); //it allows us to parse the incoming request, allows to access our json data. middleware din ito

app.use(express.urlencoded({extended:true})); //urlencoded - this is a middleware of our app - it parses the form data. extended:true - allows us 

// Defines the "/users" string to be included for all user routes defined in the "userRoutes" file
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

app.listen(process.env.PORT || 4000,() => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})