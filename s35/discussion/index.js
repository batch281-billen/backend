const express = require("express"); //para magamit si express, irerequire sya then ilalagay sa const with the name express(variable) din
const mongoose = require("mongoose"); //need din irequire si mongoose, then need to save it sa constant para constant yung paggamit sa dependency

const app = express();
const port = 3001;

// MongoDB connection
// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://bvielbillen:FxbbfCP6kG4mI8ST@wdc028-course-booking.vrtxiuq.mongodb.net/b281_to-do?retryWrites=true&w=majority",
	{
		// Deprecators - nagbibigay ng deprecation warnings if may problem sa connection sa mongoDB or database
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

// Set notification for connection success or failure
let db = mongoose.connection; //eto yung maghahandle kung magkaron ng error or what then nilagay lang sa db(database) variable

// If a connection error occurred, output in the console(terminal)
db.on("error", console.error.bind(console, "connection error"));

// If the connection is successfule, output in the console
db.once("open", () => console.log("We're connected to the cloud database"));

// Mongoose Schemas - this will determine the blueprint of the mongoose document
const taskSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
})

// Models [Task models]
const Task = mongoose.model("Task", taskSchema);

// Allows the app to read json data
app.use(express.json());
// Allows the app to read data from forms
app.use(express.urlencoded({extended:true}));

// Creation of todo list routes

// Creating a new task (using POST method)
app.post("/tasks", (req, res) => {
	
	Task.findOne({name : req.body.name}).then((result, err) => {
		// If a document was found and document's name matches the information from the client (kapag may duplicate)
		if(result != null && result.name == req.body.name){
		// Return a message to the client/Postman
		return res.send("Duplicate task found");
		}
		// If no document was found
		else{
			// Create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name
			});

			newTask.save().then((savedTask, saveErr) => {
				// If there are errors in saving
				if(saveErr){
					return console.error(saveErr);
				}
				// If no error found while creating the document
				else{
					return res.status(201).send("New task created");
				}
			})
		}
	})
})

// Get all the tasks (using GET method)
app.get("/tasks", (req, res) => {
	Task.find({}).then((result, err) => {
		// If an error occurred
		if(err){
			// Will print any errors found in the console
			return console.log(err);
		}
		// If no errors are found
		else{
			return res.status(200).json({
				data : result
			})
		}
	})
})


// ACTIVITY
// Create a User schema
const userSchema = new mongoose.Schema({
	name : String,
	status : {
		type : String,
		default : "pending"
	}
})

// Create a User model
const User = mongoose.model("User", userSchema);

// Create a POST route that will access the "/signup" route that will create a user
app.post("/signup", (req, res) => {
	User.findOne({userName : req.body.userName}).then((result, err) => {
		if(result !== null && result.userName === req.body.userName){
			return res.send("Duplicate user found");
		}
		else{
			let newUser = new User({
				userName : req.body.userName,
				password : req.body.password
			});

			newUser.save().then((savedUser, saveErr) => {
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New user registered");
				}
			})
		}
	})
})

// Listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));