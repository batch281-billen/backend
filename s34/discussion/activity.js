const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Create a GET route that will access the /home route that will send a response with a simple message ("Welcome to the home page")
app.get("/home", (req, res) => {
	res.send("Welcome to the home page");
});

// Create a GET route that will access the /users route that will send the users array as a response
let users = [
	{ username: "johndoe", password: "johndoe1234" }
];

app.get("/users", (req, res) => {
	res.json(users);
});

// Create a DELETE route that will access the /delete-user route to remove a user from the mock database
app.delete("/delete-user", (req, res) => {
	// Create a variable to store the message to be sent back to the client/postman
	let message;
	if (users.length > 0) {
		for (let i = 0; i < users.length; i++) {
			if (req.body.username === users[i].username) {
				users.splice(i, 1);
				message = `User ${req.body.username} has been deleted.`;
				break;
			}
		}
		if (!message) {
			message = "User does not exist.";
		}
	} else {
		message = "No users found."
	}
	res.send(message);
});

app.listen(port, () => console.log(`Server is running at port ${port}`));