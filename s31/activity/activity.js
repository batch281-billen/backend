// 1. What directive is used by Node.js in loading the modules it needs?
// Answer: the 'require' directive



// 2. What Node.js module contains a method for server creation?
// Answer: the 'http' module


// 3. What is the method of the http object responsible for creating a server using Node.js?
// Answer: the 'createServer()' method


// 4. What method of the response object allows us to set status codes and content types?
// Answer: the 'response.writeHead()' method
	


// 5. Where will console.log() output its contents when run in Node.js?
// Answer: at the terminal window


// 6. What property of the request object contains the address's endpoint?
// Answer: the 'request.url' property