//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.

// Get Single To Do [Sample]
async function getSingleToDo(){

    return await (

       //add fetch here.
       
       fetch('<urlSample>')
       .then((response) => response.json())
       .then((json) => json)
   
   
   );

}



// Getting all to do list item
async function getAllToDo(){

   return await (

      //add fetch here.
      fetch('https://jsonplaceholder.typicode.com/todos')
      .then((response) => response.json())
      .then((json) => {
         const titles = json.map((item) => item.title);
         return titles;
      })
  );
}

getAllToDo().then((titles) => {
   console.log(titles);
});

// [Section] Getting a specific to do list item
async function getSpecificToDo() {
   return await (
      fetch('https://jsonplaceholder.typicode.com/todos/1')
         .then((response) => response.json())
         .then((json) => { return json; })
   );
}

getSpecificToDo()
   .then((data) => {
      console.log(data);
   });


// [Section] Creating a to do list item using POST method
async function createToDo() {
   return await (
      fetch('https://jsonplaceholder.typicode.com/todos', {
         method: 'POST',
         headers: {
            'Content-Type': 'application/json',
         },
         body: JSON.stringify({
            title: 'Created To Do List Item',
            userId: 1
         }),
      })
      .then((response) => response.json())
      .then((json) => { return json; })
   );
}

createToDo()
   .then((data) => {
      console.log(data);
   });

// [Section] Updating a to do list item using PUT method
async function updateToDo(){
   return await (
      fetch('https://jsonplaceholder.typicode.com/todos/1', {
         method: 'PUT',
         headers: {
            'Content-Type': 'application/json',
         },
         body: JSON.stringify({
            title: 'Updated To Do List Item',
            description: 'To update the my to do list with a different data structure',
            status: 'Pending',
            dateCompleted: 'Pending',
            userId: 1
         }),
      })
      .then((response) => response.json())
      .then((json) => { return json; })
   );
}

updateToDo()
   .then((data) => {
      console.log(data);
   });



// [Section] Deleting a to do list item
async function deleteToDo(){
   return await (
      fetch('https://jsonplaceholder.typicode.com/todos/1', {
         method: 'DELETE',
      })
   );
}




//Do not modify
//For exporting to test.js
try{
   module.exports = {
       getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
       getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
       getSpecificToDo: typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
       createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
       updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
       deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
   }
} catch(err){

}

