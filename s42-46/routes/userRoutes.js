const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");


// Route for checking if the email already exists in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for user registration
/*router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});*/

router.post("/register", async (req, res) => {
	try {
		const resultFromController = await userController.registerUser(req.body);
		res.send(resultFromController);
	} catch (error) {
		console.error('Error in user registration:', error);
		res.sendStatus(500);
	}
});



// Route for authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({ userId : userData.id}).then(resultFromController => res.send(resultFromController));
});


// Route for creating user's order (authenticated non admin user can create order)
router.post("/checkout", auth.verify, (req, res) => {

/*	const data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		productId : req.body.productId,
		productName : req.body.productName,
		quantity : req.body.quantity,
		price : req.body.price
	};*/

	const data = {
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		productId : req.body.productId,
		productName : req.body.productName,
		price : parseFloat(req.body.price),
		quantity : req.body.quantity
	};

	userController.checkout(data).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving user details (authenticated user)
router.get("/:userId/userDetails", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});



// Route for retrieving all user's orders (ADMIN only)
router.get("/orders", auth.verify, (req, res) => {

	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin == true) {
		userController.getOrders().then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
});



// Route for retrieving user's orders (validates user is NOT an ADMIN)
router.get("/myOrders", auth.verify, (req, res) => {

	const data = {
		userId : auth.decode(req.headers.authorization).id
	}

	if (data.id == true) {
		userController.getMyOrders().then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
});


module.exports = router;