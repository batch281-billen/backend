const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");


// Route for checking if the product already exist in the database
router.post("/checkProduct", (req, res) => {
  productController.checkProductExists(req.body)
    .then(resultFromController => res.json(resultFromController)) // Use res.json()
    .catch(error => res.status(500).json({ error: "Server error" }));
});

/*router.post("/checkProduct", (req, res) => {
	productController.checkProductExists(req.body).then(resultFromController => res.send(resultFromController));
});*/

// Route for creating a product (Admin Only)
router.post("/addProduct", auth.verify, (req, res) => {
  const data = {
    product: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  }

  productController.addProduct(data)
    .then(resultFromController => {
      if (resultFromController) {
        res.json({ name: resultFromController.name }); // Send the product name on success
      } else {
        res.status(400).json({ error: "Failed to create product" }); // Send error on failure
      }
    })
    .catch(error => res.status(500).json({ error: "Server error" }));
});

/*router.post("/addProduct", auth.verify, (req, res) => {

	const data = {
		product : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromController => res.send(resultFromController))
		.catch(error => res.status(403).send(error));
});
*/

// Route for retrieving all the products
router.get("/allProducts", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving all the ACTIVE products
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving a SINGLE product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for updating a product (Admin Only)
router.put("/:productId", auth.verify, (req, res) => {
	
	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin == true) {
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
});


// Route for ARCHIVING a product (Admin Only)
router.put("/:productId/archive", auth.verify, (req, res) => {

	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin == true) {
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
});


// Route for ACTIVATING a product (Admin Only)
router.put("/:productId/activate", auth.verify, (req, res) => {

	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin == true) {
		productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}
});




module.exports = router;