const Product = require("../models/Product");


// Check if product already exists

/*module.exports.checkProductExists = (reqBody) => {
	return Product.find({ name : reqBody.name }).then(result => {

		// If a match is found
		if (result.length > 0) {
			return true;

		// If no duplicate emails found
		} else {
			return false;
		};
	});
};*/

module.exports.checkProductExists = (reqBody) => {
  return Product.exists({ name: reqBody.name });
};


// Create Product (Admin Only)

module.exports.addProduct = async (data) => {
  if (!data.isAdmin) {
    return Promise.reject("Unauthorized: Only admin users can add products.");
  }

  const existingProduct = await Product.findOne({ name: data.product.name });

  if (existingProduct) {
    return false; // Product already exists
  } else {
    const newProduct = new Product({
      name: data.product.name,
      description: data.product.description,
      price: data.product.price,
      stockQty: data.product.stockQty
    });

    try {
      const product = await newProduct.save();
      return product;
    } catch (error) {
      console.error("Error saving product:", error);
      return false;
    }
  }
};

/*module.exports.addProduct = (data) => {
  if (!data.isAdmin) {
    return Promise.reject("Unauthorized: Only admin users can add products.");
  }

  return Product.findOne({ name: data.product.name }).then((existingProduct) => {
    if (existingProduct) {
      throw new Error("Product already registered.");
    } else {
      let newProduct = new Product({
        name: data.product.name,
        description: data.product.description,
        price: data.product.price,
        stockQty: data.product.stockQty
      });

      return newProduct.save().then((product) => {
        return true;
      });
    }
  }).catch((error) => {
    console.error("Error adding product:", error);
    return false;
  });
};*/


// Retrieve all products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};


// Retrieve all ACTIVE products
module.exports.getAllActive = () => {
	return Product.find({ isActive : true }).then(result => {
		return result;
	});
};


// Retrieve a SINGLE product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	});
};


// Update product info (Admin Only)
/*
module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
		stockQty : reqBody.stockQty
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {

		if (error) {
			return false;
		} else {
			// Update stock qty based on ordered qty of user
			if (reqBody.userOrders && reqBody.userOrders.quantity) {
				return Product.findByIdAndUpdate(
					reqParams.productId,
					{ $inc: { stockQty: -reqBody.userOrders.quantity } },
					{ new: true }
				)
					.then((updatedProduct) => {
						// Update orderedProducts in User Schema
						return User.updateMany(
							{ "orderedProducts.products.productId": reqParams.productId },
							{ $set: { "orderedProducts.$[outer].products.$[inner].quantity": reqBody.userOrders.quantity } },
							{ arrayFilters: [{ "outer.products.productId": reqParams.productId }, { "inner.quantity": { $gt: reqBody.userOrders.quantity } }] }
						)
							.then(() => {
								return updatedProduct;
							})
							.catch((error) => {
								console.error("Error updating ordered products quantity:", error);
								return false;
							});

					})
					.catch((error) => {
						console.error("Error updating product stock quantity:", error);
						return false;
					});
			} else {
				return true;
			}
		}
	});
};
*/

// NEW CODE: Update product info (Admin Only)
module.exports.updateProduct = async (reqParams, reqBody) => {
  try {
    const { name, description, price, stockQty } = reqBody;

    const updatedProduct = await Product.findByIdAndUpdate(
      reqParams.productId,
      {
        name,
        description,
        price,
        stockQty
      },
      { new: true }
    );

    // Update stock quantity based on ordered quantity of user
    if (reqBody.userOrders && reqBody.userOrders.quantity) {
      await Product.findByIdAndUpdate(
        reqParams.productId,
        { $inc: { stockQty: -reqBody.userOrders.quantity } }
      );
    }

    return updatedProduct;
  } catch (error) {
    console.error("Error updating product:", error);
    return { success: false, message: "Error updating product." };
  }
};



// Archiving a product (Admin Only)

module.exports.archiveProduct = async (reqParams) => {
  try {
    let updateActiveField = {
      isActive: false,
    };

    await Product.findByIdAndUpdate(reqParams.productId, updateActiveField);

    // Product ARCHIVED successfully
    return { success: true, message: "Product archived successfully." };
  } catch (error) {
    console.error("Error archiving product:", error);
    return { success: false, message: "Error archiving the product." };
  }
};


/*module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

		// Product not archived
		if (error) {
			return (false, 'Error archiving the product.');
		
		// Product ARCHIVED successfully
		} else {
			return (true, 'Product archived successfully.');
		}
	});
};*/


// Activating a product (Admin Only)
module.exports.activateProduct = async (reqParams) => {
  try {
    let activateActiveField = {
      isActive: true,
    };

    await Product.findByIdAndUpdate(reqParams.productId, activateActiveField);

    // Product is now active
    return { success: true, message: "Product is now active." };
  } catch (error) {
    console.error("Error activating product:", error);
    return { success: false, message: "Error activating the product." };
  }
};


/*module.exports.activateProduct = (reqParams) => {

	let activateActiveField = {
		isActive : true
	};

	return Product.findByIdAndUpdate(reqParams.productId, activateActiveField).then((product, error) => {

		if (error) {
			return (false, 'Error activating the product.');
		} else {
			return (true, 'Product is now active.');
		}
	});
};*/