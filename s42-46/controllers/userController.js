const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Check if email already exists

module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email : reqBody.email }).then(result => {

		// If a match is found
		if (result.length > 0) {
			return true;

		// If no duplicate emails found
		} else {
			return false;
		};
	});
};


// User registration

/*module.exports.registerUser = (reqBody) => {

	let newUser = new User ({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {

		// User registration failed
		if (error) {
			return false;

		// User registration successful
		} else {
			return true;
		}
	});

	return User.find({email : reqBody.email}).then(result => {
		if (result.length > 0) {
			return true
		}
	})
};*/

module.exports.registerUser = async(reqBody) => {
	try {
		// Check if the user already exists
		const existingUser = await User.findOne({ email : reqBody.email });
		if (existingUser) {
			return (false, 'User already registered.');
		}

		// Creates a variable named "newUser" and insantiates a new "User" object using the Mongoose model
		const newUser = new User({
			firstName : reqBody.firstName,
			lastName : reqBody.lastName,
			email : reqBody.email,
			password : bcrypt.hashSync(reqBody.password, 10)
		});

		// Save the new user
		await newUser.save();
		return true;
	} catch (error) {
		console.error('Error during registration.', error);
		return false;
	}
};

// User authentication

module.exports.loginUser = (reqBody) => {

	return User.findOne({ email : reqBody.email }).then(result => {

		// If User does not exist
		if (result == null) {
			return false;
		
		// User exists
		} else {
			
			// For password comparison (encrypted vs non-encrypted password)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// If password matches
			if (isPasswordCorrect) {

				return {access : auth.createAccessToken(result)};
			
			// If password do not match
			} else {
				return false;
			}

		}
	});
};


// Retrieving user details

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		return result;
	});
};


// Creating user's order
// Need to use async await method since 2 separate documents needs to be updated upon creating the order.


/*module.exports.checkout = async(data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		// Adds the productId in the user's orderedProduct array
		user.orderedProducts.push({productId : data.productId});

		// Saves the updated user info in the database
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		});
	});

	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.userOrders.push({userId : data.userId});

		return product.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});

	// If both conditional statement is satisfied or equals to true
	if (isUserUpdated && isProductUpdated) {
		return true;

	// Creating order failure
	} else {
		return false;
	}
};
*/


module.exports.checkout = async(data) => {
	try {
		// Find the user by userId
		const user = await User.findById(data.userId);

		// Create new order
		const newOrder = {
			products: {
				productId : data.productId,
				productName : data.productName,
				price: data.price,
				quantity : data.quantity,
			},
			totalAmount : data.quantity * data.price,
			purchasedOn : new Date(),
		};

		// Push the new order to user orderedProducts array
		user.orderedProducts.push(newOrder);
		// user.orderedProducts = [...user.orderedProducts, newOrder];


		// Save the updated user info
		await user.save();

		// Update product's userOrders array
		// const product = await Product.findById(data.productId);
		// product.userOrders.push({userId: data.userId, quantity: data.quantity, purchasedOn: data.purchasedOn});
		
		// await product.save();

		// UPDATED CODE: Update product's stock quantity
		await Product.findByIdAndUpdate(data.productId, 
			{ $inc: { stockQty: -data.quantity },
		});


		return { success : true, message : 'Order created successfully!' };
	} catch (error) {
		return { success : false, message : 'Error creating order.', error};
	}
};


// Retrieve user details

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};


// Retrieving all users orders (Admin only)
module.exports.getOrders = () => {

	return User.find({isAdmin : false}).then(result => {
		return result;
	});
};


// Retrieving user's orders (validates user is not an admin)
module.exports.getMyOrders = () => {

	return User.find({userId : true}).then(result => {
		return result;
	});
};