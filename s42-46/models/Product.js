const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

	name : {
		type : String,
		required : [true, "Product name is required."]
	},
	description : {
		type : String,
		required : [true, "Description is required."]
	},
	price : {
		type : Number,
		required : [true, "Price is required."]
	},
	stockQty : {
		type : Number,
		default : 0
	},
	isActive : {
		type : Boolean,
		default : true 
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	userOrders : [
		{
			userId : {
				type : mongoose.Schema.Types.ObjectId,
				ref : "User",
				required : [true, "UserId is required."]
			},
			quantity : {
				type : Number,
				default : 0
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			}
		}
	]
});

module.exports = mongoose.model("Product", productSchema);