const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First Name is required."]
	},
	lastName : {
		type : String,
		required : [true, "Last Name is required."]
	},
	email : {
		type : String,
		required : [true, "Email is required."]
	},
	password : {
		type : String,
		required : [true, "Password is required."]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	orderedProducts : [
		{
			products : [
				{
					productId : {
						type : mongoose.Schema.Types.ObjectId,
						ref : "Product",
						required : [true, "Product Id is required."]
					},
					productName : {
						type : String,
						required : [true, "Product name is required."]
					},
					price : {
						type : Number,
						required : [true, "Quantity is required."]
					},
					quantity : {
						type : Number,
						required : [true, "Quantity is required."]
					}
				}
			],
			totalAmount : {
				type : Number,
				default : 0
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			}
		}
	]
});

module.exports = mongoose.model("User", userSchema);