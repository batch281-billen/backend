const jwt = require("jsonwebtoken");
const secret = "CookieOrdersAPI";


// JSON WEB TOKENS

module.exports.createAccessToken = (user) => {
	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
	};

	return jwt.sign(data, secret, {});
};


// Token Verification

module.exports.verify = (req, res, next) => {

	// The token is retrieved from the request header (Postman > Authorization > Bearer Token)
	let token = req.headers.authorization;

	// Token received and is not undefined
	if (typeof token !== "undefined") {
		console.log(token);

		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {

			// If JWT is not valid
			if (err) {
				return res.send({auth : "failed"})

			} else {
				next();
			}
		})
	// If Token does not exist
	} else {
		return res.send({auth : "failed"});
	};
};


// Token decryption
module.exports.decode = (token) => {

	// Token received and is not undefined
	if (typeof token !== "undefined") {

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if (err) {
				return null;
			} else {
				return jwt.decode(token, {complete:true}).payload;
			}
		})
	} else {
		return null;
	};
};