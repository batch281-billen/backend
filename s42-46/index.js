const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

const app = express();


// MongoDB connection
mongoose.connect("mongodb+srv://bvielbillen:FxbbfCP6kG4mI8ST@wdc028-course-booking.vrtxiuq.mongodb.net/capstone2?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, "MongoDB connection Error."));
db.once('open', () => console.log('You are now connected to MongoDB Atlas!'));


// Backend Application Access
app.use(cors());

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Routes access
app.use("/users", userRoutes);
app.use("/products", productRoutes);


app.listen(process.env.PORT || 4000,() => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});