// Comments

// Comments are part of the code that gets ignored by the language
// Comments are meant to describe the written code.

/* 
	Two types of comments
	1. Single line - Ctrl + /
	2. Multi line - Ctrl + Shift + /
*/

console.log("Hello, World!");

// Variables
/*
	- it is used to contain data.
*/

// Declaring Variables - tells our devices that a variable name is created and is ready to store data.

/*
	Syntax:
		let/const variableName; [firstName]

		let - keyword usually used in declaring a variable.
		const - keyword usually used in declaring CONSTANT variable
*/

let myVariable;
console.log(myVariable);

// console.log(hello);

// Initializing variables - the instance when a variable is given it's initial/starting value. nagdedeclare ng variable at the same time nagdedeclare ng initial value.
/*
	Syntax:
		let/const variableName = value;
*/
let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;

// Reassigning variable values
// Syntax: variableName = newValue;

productName = 'laptop';
console.log(productName);

// interest = 4.489;

// Declares a variable first
let supplier;
// Initialization of value
supplier = "John Smith Tradings";

// Multiple variable declaration

let productCode = 'DC017', productBrand = 'Dell';
console.log(productCode, productBrand);

// Data Types

// Strings - a data type that handles a series of characters that create a word, phrase, or a sentence. 
let country = 'Philippines';
let province = "Metro Manila";

// Concatenating strings (+ is used)
let fullAddress = province + 
', ' + country;
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

// Escape Character (\) in strings is combination with other characters can produce a different effect (\n)
let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

let message = "John's employees went home early";
console.log(message);

// Numbers
// Integers/whole numbers
let headcount = 32;
console.log(headcount);

// Decimal numbers/fractions
let grade = 98.7;
console.log(grade);

// Exponential notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text/numbers and strings
console.log("John's grade last quarter is " + grade);

// Boolean - used to store values relating to the state of certain things. answerable with true or false.
let isMarried = true;
let inGoodConduct = false;
console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// Array - are special kind of data type that's used to store multiple values; inside [] square bracket
// Syntax: let/const arrayName = [elementA, elementB, elementC, ...];
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// Objects - another special kind of data type that's used to mimic real world objects/items. inside {} curly bracket
/*
	Syntax:
	let/const objectName = {
		propertyA : value,
		propertyB : value (dapat wala na ito comma since last property na)
	}
*/
let person = {
	fullName : 'Juan Dela Cruz',
	age : 35,
	isMarried : false,
	contact : ["0917 123 4567", "8123 4567"],
	address : {
		houseNumber : '345',
		city : 'Manila'
	}
}

console.log(person);

let myGrades = {
	firstGrading : 98.7,
	secondGrading : 92.1,
	thirdGrading : 90.2,
	fourthGrading : 94.6
}

console.log(myGrades);

// 'typeof' operator - used to determining the type of data or the value of the variable.
console.log(typeof myGrades);

console.log(typeof grades);

// Null - used to intentionally express the absence of a value in a variable declaration/initialization.
let spouse = null;

let myNumber = 0;
let myString = '';

// Undefined
let fullName;
console.log(fullName);


// MOCK TECH EXAM DAY 2 PART 1
// Identify errors in the provided code snippet.

/*
a. There is nothing wrong with the code.
b. `myObj.hobbies[2]` should be `myObj.hobbies[1]`. <ANS>
c. The sayHello method is not called anywhere.
d. The sayHello method has a syntax error.
*/

let myObj = {
	name: "John",
	age: 25,
	job: "Dev",
	hobbies: ["read", "play"],
	sayHello: function() {
		console.log("Hello, my name is" + this.name);
	}
};
console.log(myObj.hobbies[2]);



// What would be the console output of the function?

/*
a. "I was given 3 french hens"
b. "I was given 3 golden rings"
c. "I was given 3 turtle doves" <ANS>
d. No visible output
*/

function checkGift(day) {
	let gifts = [
		"partridge",
		"turtle",
		"french",
		"golden"
	];
	if (day > 0 && day < 4) {
		return `I was given ${day} ${gifts[day-1]}`;
	} else {
		return "No gifts";
	}
}

checkGift(3);


// What would be the output?

/*
a. Current row: 1, Current col: 1; Current row: 2, Current col: 1; Current row: 2, Current col: 2;
b. Current row: 1, Current col: 1; Current row: 2, Current col: 2;
c. Current row: 1, Current col: 1; Current row: 1, Current col: 2; Current row: 2, Current col: 1; Current row: 2, Current col: 2; <ANS>
d. no output
*/

for (let row = 1; row < 3; row++) {
	for (let col = 1; col <= row; col++) {
		console.log(`Current row: ${row}, current col: ${col}`);
	}
}


// What is the output of the code below?

/*
a. True <ANS>
b. False
c. undefined
d. This code will produce an error.
*/

function isEven(num) {
	if (num % 2 === 0) {
		return true;
	} else {
		return false;
	}
}

console.log(isEven(4));


// What is wrong with the following code in JavaScript?
/*
a. There is nothing wrong with the code.
b. The `for` loop is missing a closing brace. <ANS>
c. The `for` loop should use `let i = 1` instead of `let i = 0`.
d. The `for` loop should not increment `i` inside the loop body.
*/

const nums = [1, 2, 3, 4, 5];
for (let i = 0; i < nums.length; i++) {
	console.log(nums[i]);
	i++
}


// Identify issues or errors in the provided code snippet.
/*
a. The function declaration is missing parentheses around the parameter.
b. The function does not return a value.
c. The variable result is not declared with var, let, or const. <ANS>
*/

function square(x) {
	x * x;
}

const result = square(5);
console.log(result);

// What is the output of the following code?
/*
a. 5
b. 10
c. TypeError <ANS>
d. SyntaxError
*/

const myFunction = () => {
	const x = 5;
	x = 10;
	console.log(x);
}

myFunction();

// Given the array below, how can the last student's English grade be displayed?
/*
a. console.log(records[2].subjects[0].grade)
b. console.log(records[3].subjects[1].grade)
c. console.log(Junson.English.grade)
d. console.log(records[2].subjects[1].grade) <ANS>
*/

let records = [
	{
		id: 1,
		name: 'Brandon',
		subjects: [
			{ name: 'English', grade: 98 },
			{ name: 'Math', grade: 66 },
			{ name: 'Science', grade: 87 }
		]
	},
	{
		id: 2,
		name: 'Jobert',
		subjects: [
			{ name: 'English', grade: 87 },
			{ name: 'Math', grade: 99 },
			{ name: 'Science', grade: 74 }
		]
	},
	{
		id: 3,
		name: 'Junson',
		subjects: [
			{ name: 'English', grade: 60 },
			{ name: 'Math', grade: 99 },
			{ name: 'Science', grade: 87 }
		]
	}
];

// What would be the problem in the code snippet?
/*
a. There will be no output
b. There will be an extra *undefined* in the console <ANS>
c. There will be missing students
d. There will be no problems
*/

let students = ['John', 'Paul', 'George', 'Ringo'];

console.log('Here are the graduating students:');

for (let count = 0; count <= students.length; count++) {
	console.log(students[count]);
}

// What is the output of the following code?
/*
a. 10
b. 20
c. 25
d. NaN <ANS>
*/
const num = 5;
function multiplyByTwo() {
	const num = 10;
	return num * 2;
}

console.log(multiplyByTwo());

// What is wrong with the following code
/*
a. There is nothing wrong with the code.
b. The variable foo is not declared with let or var.
c. The variable foo is a constant and cannot be reassigned. <ANS>
d. The code will throw a syntax error because of the use of const.
*/

const foo = 1;
foo = 2;


// What would be the problem in the code snippet?
/*
a. The output will always be "Not a leap year"
b. There will be a syntax error <ANS>
c. The output will always be "Leap year"
d. There will be no problems
*/

function checkLeapYear(year) {
	if (year % 4 = 0) {
		if (year % 100 = 0) {
			if (year % 400 = 0) {
				console.log('Leap year');
			} else {
				console.log('Not a leap year');
			}
		} else {
			console.log('Leap year');
		}
	} else {
		console.log('Not a leap year');
	}
}

checkLeapYear(1999);

//What is wrong with the following code?
/*
a. There is nothing wrong with the code. <ANS>
b. The function is not defined correctly.
c. The console.log statement is not using the correct variable name.
d. The function is not returning the correct result.
*/

function calculateSum(a, b) {
	let sum = a + b;
	return sum;
}

let result = calculateSum(2, 3);
console.log("The sum of 2 and 3 is: " + result);


// What is the output of the code below?
/*

*/